// Hack to get Converse working
var _interrupt = false;

// KaiOS doesn't seem to support "defer" on a
// script tag, so we wait till the page has
// loaded to add the actual script.
function getGoing() {
  var scriptEl = document.createElement('script')
  scriptEl.type = 'text/javascript'
  scriptEl.src = '/build/bundle.js'
  scriptEl.crossOrigin = true

  document.getElementsByTagName('head')[0].appendChild(scriptEl)
  console.debug('Script element mounted')
}

// Start it when the page loads
window.onload = getGoing
