import {
  writable,
  readable,
} from 'svelte/store'
import { storable } from 'svelte-storable'
import { _converse } from '@converse/headless/core'

export const titleStore = writable('Convo')
export const softkeysStore = writable({
  left: {
    label: 'Exit',
    callback: function() { console.log('You clicked on SoftLeft') },
  },
  center: {
    label: null,
    callback: function() { console.log('You clicked on Enter') },
  },
  right: {
    label: null,
    callback: function() { console.log('You clicked on SoftRight') },
  }
})

// XMPP info
export const xmppConnected = readable(false, function start(set) {
  let interval = setInterval(() => {
    let status = _converse.api.connection.connected() || false
    set(status)

    if (status) clearInterval(interval)
  }, 1000)

  return function stop() {
    clearInterval(interval)
  }
})

export const xmppChatboxes = readable([], function start(set) {
  let interval = setInterval(() => {
    if (_converse.chatboxes?.length) {
      set([..._converse.chatboxes])
     }
  }, 1000)

  return function stop() {
    clearInterval(interval)
  }
})

// Login stuff (important and convenient!)
export const username = storable('xmpp-username')
export const password = storable('xmpp-password')
export const useAdvancedSettings = storable('xmpp-bosh-url')
export const boshURL = storable('xmpp-bosh-url')
export const wsURL = storable('xmpp-websocket-url')