import { converse, _converse } from '@converse/headless/core';
import "@converse/headless/plugins/adhoc.js";            // XEP-0050 Ad Hoc Commands
import "@converse/headless/plugins/bookmarks/index.js";  // XEP-0199 XMPP Ping
import "@converse/headless/plugins/bosh.js";             // XEP-0206 BOSH
import "@converse/headless/plugins/caps/index.js";       // XEP-0115 Entity Capabilities
import "@converse/headless/plugins/carbons.js";          // XEP-0280 Message Carbons
import "@converse/headless/plugins/chat/index.js";       // RFC-6121 Instant messaging
import "@converse/headless/plugins/chatboxes/index.js";
import "@converse/headless/plugins/disco/index.js";      // XEP-0030 Service discovery
import "@converse/headless/plugins/headlines.js";        // Support for headline messages
import "@converse/headless/plugins/mam/index.js";        // XEP-0313 Message Archive Management
import "@converse/headless/plugins/muc/index.js";        // XEP-0045 Multi-user chat
import "@converse/headless/plugins/ping/index.js";       // XEP-0199 XMPP Ping
import "@converse/headless/plugins/pubsub.js";           // XEP-0060 Pubsub
import "@converse/headless/plugins/roster/index.js";     // RFC-6121 Contacts Roster
import "@converse/headless/plugins/smacks/index.js";     // XEP-0198 Stream Management
import "@converse/headless/plugins/status/index.js";
import "@converse/headless/plugins/vcard/index.js";      // XEP-0054 VCard-temp

import "@converse/headless/plugins/emojis/index.js";      // Emojis

// We're just importing these to activate the addon
import {
    convertASCII2Emoji,
    getShortnameReferences,
    getCodePointReferences
} from '@converse/headless/plugins/emoji/utils.js';
import {
    appendArrayBuffer,
    arrayBufferToBase64,
    arrayBufferToHex,
    arrayBufferToString,
    base64ToArrayBuffer,
    hexToArrayBuffer,
    stringToArrayBuffer
} from '@converse/headless/utils/arraybuffer.js';

// Define unescapeHTML since it's needed by a plugin
// This function is there in ConverseJS but not included
// in the headless version
import u from '@converse/headless/utils/core'

/**
 * Helper method that replace HTML-escaped symbols with equivalent characters
 * (e.g. transform occurrences of '&amp;' to '&')
 * @private
 * @method u#unescapeHTML
 * @param { String } string - a String containing the HTML-escaped symbols.
 */
u.unescapeHTML = function (string) {
    var div = document.createElement('div');
    div.innerHTML = string;
    return div.innerText;
};

u.escapeHTML = function (string) {
    return string
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
};


// Expose converse for debugging
// TODO: remove this before release!
window.converse = converse;
window._converse = _converse;

/**
 * Once Converse.js has loaded, it'll dispatch a custom event with the name `converse-loaded`.
 * You can listen for this event in order to be informed as soon as converse.js has been
 * loaded and parsed, which would mean it's safe to call `converse.initialize`.
 * @event converse-loaded
 * @example window.addEventListener('converse-loaded', () => converse.initialize());
 */
const ev = new CustomEvent('converse-loaded', { detail: { converse } });
window.dispatchEvent(ev);

// Set up custom plugins
converse.plugins.add('convo', {
  initialize: function () {
    const {_converse} = this
    const log = _converse.log

    _converse.api.listen.on('disconnected', () => {
      console.debug('We disconnected :/')
    })

    _converse.api.listen.on('initialized', () => {
      console.debug('The connection has now been initialised! :D')
    })

    _converse.api.listen.on('connected', () => {
      console.debug('Connected successfully ;)')

      _converse.api.listen.on('message', (msg) => {
        console.debug(`${msg.attrs.from} says: ${msg.attrs.body}`)
      })
    })


    _converse.api.listen.on('pluginsInitialized', function () {
        // We only register event handlers after all plugins are
        // registered, because other plugins might override some of our
        // handlers.
        //_converse.api.listen.on('message', m => console.log('message', m));

        console.debug('Handlers ready!')

        // emoji don't seem to be getting initialized,
        // so let's do it manually
        _converse.api.emojis.initialize()
    })
  },
})

export default converse;
