<div align="center">

![Convo logo](https://git.disroot.org/badrihippo/convo/raw/branch/master/public/images/icon-256.png)

# Convo

A (simple, very basic) Jabber/XMPP client for [KaiOS](https://www.kaiostech.com/), powered by [ConverseJS](https://conversejs.org)

[![Discuss via XMPP](https://img.shields.io/badge/discuss-via_xmpp-8a2be2?logo=xmpp&link=https%3A%2F%2Fjoin.jabber.network%2F%23convo%40chat.disroot.org%253Fjoin)](https://join.jabber.network/#convo@chat.disroot.org?join)
[![Liberapay goal progress](https://img.shields.io/liberapay/goal/convo?logo=liberapay)](https://liberapay.com/convo/donate)
[![Open issues](https://git.disroot.org/badrihippo/convo/badges/issues/open.svg?logo=forgejo&color=fb923c)](https://git.disroot.org/badrihippo/convo/issues)
[![Releases](https://git.disroot.org/badrihippo/convo/badges/release.svg?logo=git&color=turquoise)](https://git.disroot.org/badrihippo/convo/releases)

[<img height="60" alt="Get it on the BananaHackers WebStore" src="badge-bananahackers.png"/>](https://store.bananahackers.net/#Convo)

</div>

<details>
<summary>Screenshots</summary>

![Splash screen](screenshots/splash.png)
![Login screen](screenshots/login.png)
![First start (or waiting to load) screen](screenshots/blank.png)
![Contact list](screenshots/contacts.png)
![Inside a (group) conversation](screenshots/groupchat.png)
![Option to close the chat](screenshots/close-chat.png)
![Populated conversation list, with profile pictures!](screenshots/messages.png)
![A (formerly) exciting announcement](screenshots/groupchats-coming-soon.png)

</details>

# Installation

This app is published to the BananaHackers WebStore. So if you have [the BananaHackers WebStore client](https://store.bananahackers.net/#storeclient), you can install Convo directly by searching for it or [open the page on your computer](https://store.bananahackers.net/#Convo) to QR-beam it to your phone.

If you don't have the BananaHackers store, you can download the latest release as a .zip file from [the Releases page](https://git.disroot.org/badrihippo/convo/releases).

To install the .zip file, you will have to use one of the following methods:

- If you have an older device, you can simply install the .zip using [OmniSD](https://wiki.bananahackers.net/en/guides/omnisd), assuming you have set that up.
- Alternatively, you can: extract the .zip file, extract the `application.zip` file you find inside (a zip-in-a-zip!) and sideload the resulting folder using [ADB and WebIDE](https://wiki.bananahackers.net/development/webide) (for KaiOS 2.x) or [the official appscmd tool](https://developer.kaiostech.com/docs/sfp-3.0/getting-started/env-setup/os-env-setup/) (for KaiOS 3.x)
- Finally, if you want to get the latest changes you can clone the repo and build it yourself as described in the next section.

# Development and testing

`yarn dev` builds the app in watch mode and serves the site. Great for testing
it out in a desktop browser.

## Enabling XMPP stanza printing

If you want Convo to print out all the XMPP stanzas
to the console as they come in, you can open the file
`src/routes/Login.svelte`, and search for a line looking
like:

```
        loglevel: 'warn',
```

Edit that line to replace `warn` with `debug`

```
        loglevel: 'debug',
```

In future, this and other options in that code block
will be made into settings that can be toggled from
within the app itself, for easier access.

## Deploying to a device

1. Connect your device to your computer and make sure it appears in WebIDE.
2. `yarn build`
3. In WebIDE, load the `/public` folder as a packaged app.

# Contributing

Pull requests are welcome. Feel free to pick up any of the [issues](https://git.disroot.org/badrihippo/convo/issues) or work on your own feature. (If it's some wacky feature, maybe discuss it first by making a new issue!)

The good news is that Convo is based on [ConverseJS](https://conversejs.org), which already supports a wide range of features. So adding a "feature" to Convo often involves just implementing the UI to expose a pre-existing ConverseJS feature!

If you're not sure where to start, you can always [join the XMPP chatroom](https://join.jabber.network/#convo@chat.disroot.org?join)!

## Donations

Besides code, you can also contribute financially. I haven't been finding the time to work on Convo, so your funds would help me make more time to work on it.

I charge about 7.50 USD per hour for the freelance projects I take on, so I will dedicate a corresponding amount of time for any donations that come in. (Of course, I will also be putting in extra work when I'm able, but funding would help establish a baseline since I won't have to decide between working on this and earning my daily bread 🍞)


<a href="https://liberapay.com/convo/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

# Recommended companion apps

Since Convo is in a very early stage, it is recommended to have an additional XMPP running on your desktop, laptop, or smart(er)phon. This will let you perform operations that can't be done through Convo directly (such as setting up a bridge or accepting contact requests). Recommendations for stable apps are as follows, with a preference for stability over looks since you'll probably not be using that app as much as the KaiOS one:

- [Gajim](https://gajim.org/) - Linux, Windows
- [Conversations](https://conversations.im/) - Android
- [Monal IM](https://monal-im.org/) - iOS and MacOS
- [Dino](https://dino.im) - Linux (less group chat and moderation features, but has a responsive design to work on mobile devices)

# Credits and Licensing

- This project is based on the
  [KaiOS Svelte starter](https://github.com/kaios-community/kaios-svelte-starter) (MIT)
- UI components from [KaiOS-native-UI](https://github.com/canicjusz/KaiOS-native-UI) (WTFPL)
- XMPP/Jabber support from [ConverseJS](https://github.com/conversejs/converse.js) ([headless version](https://www.npmjs.com/package/@converse/headless), MPL-2.0)
- Icon created from a combination of [FontAwesome](https://fontawesome.io)'s [comment icon](https://fontawesome.com/icons/comment) and [Icons8](https://icons8.com)'s [XMPP icon](https://icons8.com/icon/djXoPYO4uCXD/xmpp)

The project itself is licenced under the GNU General Public Licence version 3.0, or, at your convenience, any later version. For more information, see the [`COPYING`](COPYING) file.
